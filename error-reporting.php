<?php
/*
Plugin Name:  Error Reporting
Description:  Hides deprecated errors
Version:      1.0.0
Author:       Oliver
Author URI:   http://oli.digital/
License:      MIT License
*/

if (WP_DEBUG) {
    error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
}